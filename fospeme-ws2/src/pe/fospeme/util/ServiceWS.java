package pe.fospeme.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sun.jmx.snmp.Timestamp;

import pe.fospeme.coneccion.ConnectionOracleFactory2;
import pe.gob.susalud.jr.transaccion.susalud.bean.In278ResCG;
import pe.gob.susalud.jr.transaccion.susalud.bean.In278ResCGDetalle;
import pe.gob.susalud.jr.transaccion.susalud.bean.In278SolCG;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConCod271;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConCod271Detalle;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConMed271Detalle;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConNom271Detalle;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConProc271Detalle;

public class ServiceWS {

	public String getIdRemitente(String id){
		String respuesta=null;
		 Connection connection = ConnectionOracleFactory2.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            String sql="SELECT VA_TIPOENTIDAD FROM STM_ENTIDAD where "
	            		+ "CO_ENTIDAD_N='" +id+"'";
	            ResultSet rs = stmt.executeQuery(sql);
	            if(rs.next())
	            {
	            	respuesta=rs.getString(1);
	            }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return respuesta;
		
	}
	public String getRUCRemitente(String id){
		String respuesta=null;
		 Connection connection = ConnectionOracleFactory2.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            String sql="SELECT UN_RUC FROM STM_ENTIDAD where "
	            		+ "CO_ENTIDAD_A='" +id+"'";
	            ResultSet rs = stmt.executeQuery(sql);
	            if(rs.next())
	            {
	            	respuesta=rs.getString(1);
	            }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return respuesta;
		
	}
	
	public boolean getIafas(String id){
		boolean respuesta=false;
		 Connection connection = ConnectionOracleFactory2.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            String sql="SELECT UN_RUC FROM STM_ENTIDAD where "
	            		+ "CO_ENTIDAD_A='" +id+"' or " + "CO_ENTIDAD_N='" +id+"' " ;
	            ResultSet rs = stmt.executeQuery(sql);
	            if(rs.next())
	            {
	            	respuesta=true;
	            }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return respuesta;
		
	}
	public InConCod271 getAfiliadoCOD(InConCod271 r,String cod){
		InConCod271 resDet=r;
		Connection connection = ConnectionOracleFactory2.getConnection();
		
		String sql="select af.AP_PATERNO,af.NO_NOMBRES,af.CO_AFILIADO_CIP,af.AP_MATERNO,af.IsActive, "
				+ "af.COD,af.NU_TIPO_DOCUMENTO,af.NU_CONTRATO,af.PROD_CODIGO,af.CO_FILIACION,	"
				+ "af.NU_PLAN,CO_GENERO, COALESCE(to_char (af.FE_NACIMIENTO, 'yyyymmdd'),''), af.CO_ESTADO_CIVIL, af.NU_RUC, "
				+ "con.AP_CONTRATANTE, con.AM_CONTRATANTE, con.NO_CONTRATANTE, con.CO_TIPOCONTRATANTE,con.CO_TIPODOC,"
				+ "af.CO_AFILIADO_CIP	"
				+ "from ACRTM_AFILIADOS af "
				+ "left join STD_CONTRATANTE con on con.UN_RUC=af.NU_RUC "
				+ "where  af.CO_AFILIADO_CIP=?";
		String codPro="";
		 try {
			 	PreparedStatement st = connection.prepareStatement(sql);
	        	st.setString(1, cod);
	            ResultSet rs = st.executeQuery();
	            if(rs.next())
	            {
	        			System.out.println("COMPLETANDO..");
        			resDet.setApPaternoPaciente(rs.getString(1));//16
        			resDet.setNoPaciente(rs.getString(2));
        			resDet.setCoAfPaciente(rs.getString(3));
        			resDet.setCoAfPaciente(rs.getString(13));
        			resDet.setApMaternoPaciente(rs.getString(4));
        			if(rs.getString(5).equals("Y"))
        				resDet.setCoEsPaciente("1"); //ESTADO
        			else
        				resDet.setCoEsPaciente("6"); //ESTADO
        			resDet.setTiDoPaciente(rs.getString(6)); //18
        			resDet.setNuDoPaciente(rs.getString(7));//23
        			resDet.setNuIdenPaciente(rs.getString(21));
        			if(rs.getString(11)==null)
        				resDet.setNuPlan("");//25
        			else
        				resDet.setNuPlan(rs.getString(11));//25
        			if(rs.getString(13).length()==0)
        				resDet.setFeNacimiento("20001010");//26
        			else
        				resDet.setFeNacimiento(rs.getString(13));//26
        			
        			resDet.setCoParentesco("01");//24
        			resDet.setTiPlanSalud("7");
        			resDet.setCoProducto(rs.getString(9));//21
        			codPro=rs.getString(9);
//        			resDet.setCoProducto("1");//21
        			resDet.setCoMoneda("1");
        			resDet.setFeFinVigencia("20181010");
        			resDet.setFeInsTitular("20141010");
        			resDet.setFeIniVigencia("20021010");
        			resDet.setTiCaContratante("2");
        			resDet.setTiDoTitular("1");
        			resDet.setCoAfPaciente(cod);
        			resDet.setCoTiPoliza("1");
        			resDet.setNuPoliza("1111");
	    			resDet.setCoAfTitular("25");
	    			resDet.setTiDoTitular("1");
	    			resDet.setCaTitular("2");
	    			resDet.setNuDoTitular("12345678");
	    			resDet.setNoMaTitular("mat");
	    			resDet.setNoPaTitular("pat");
//	    			resDet.setco
	    			resDet.setNuDoTitular("12345678");
        			resDet.setGenero(rs.getString(12));//27
        			resDet.setEsMarital("1");//28
        			resDet.setTiCaContratante(rs.getString(19));//29
        			resDet.setNoPaContratante(rs.getString(16));//30
        			resDet.setNoContratante(rs.getString(18));//31
        			resDet.setNoMaContratante(rs.getString(17));//32
        			resDet.setTiDoContratante(rs.getString(20));//33
        			resDet.setIdReContratante("XX5");//34
        			resDet.setCoReContratante(rs.getString(15));//35
////        			resDet.setca
//        			list.add(resDet);
	        		
	            }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		 System.out.println("completando afiliado... ok "+codPro);

			InConCod271Detalle det= null;
//			det.setcal
			boolean tieneCovertura=false;
			String sqlCovertura =" select sc.CO_TIPO_COBERTURA, sc.IN_RESTRICCION, sc.CO_SUB_TIPO_COBERTURA, m.CO_TIPO_MONEDA,sc.VA_COPAGO_FIJO, "
					+ "tc.CO_TIPO_CLASIFICACION, tc.VA_CANTIDAD_CLASIFICACION,sc.VA_COPAGO_VARIABLE,sc.IN_CARTA_GARANTIA "
					+ "from STD_SUBCOBERTURA sc "
					+ "left join STD_MONEDA m on m.STD_MONEDA_ID=sc.STD_MONEDA_ID "
					+ "left join STD_TIPO_CLASIFICACION tc on tc.STD_TIPO_CLASIFICACION_ID=sc.STD_TIPO_CLASIFICACION_ID "
					+ "where sc.CO_PRODUCTO=? ";
			
			 try {
				 	PreparedStatement st = connection.prepareStatement(sqlCovertura);
		        	st.setString(1, codPro);
		            ResultSet rs = st.executeQuery();
		            while(rs.next())
		            {
		            	det= new InConCod271Detalle();
		            	det.setNuCobertura(rs.getString(1));
		            	det.setCoInRestriccion(rs.getBigDecimal(2).intValue()+"");
		            	det.setCoTiCobertura(rs.getString(1));
		    			det.setCoSubTiCobertura(rs.getString(3));
		    			det.setCoTiMoneda(rs.getString(4));
		    			det.setCoPagoFijo(rs.getString(5));
		    			det.setCoCalServicio(rs.getString(6));
		    			if(rs.getString(7)==null)
		    				det.setCanCalServicio("0");
		    			else
		    				det.setCanCalServicio(rs.getString(7));
		    			det.setCoPagoVariable(rs.getString(8));
//		    			det.setFlagCaGarantia(rs.getString(9));
		    			if(rs.getString(9)==null)
		    				det.setFlagCaGarantia("0");
		    			else
		    				det.setFlagCaGarantia(rs.getString(9));
		    			
		    			det.setInfBeneficio("1");
		    			det.setBeMaxInicial("50");
//		    			det.setMoCobertura(moCobertura);
		    			
//		    			det.setpag //-->61
		    			det.setIdProducto("2");
		    			
//		    			det.setMsgObs(msgObs);
//		    			det.setMsgConEspeciales(msgConEspeciales);
		    			
//		    			det.setCoInRestriccion(coInRestriccion);
//		    			det.setInfBeneficio(infBeneficio);//-->73
//		    			det.setFeFinCarencia(feFinCarencia);
//		    			det.setFeFinEspera(feFinEspera);
		    			tieneCovertura=true;
		    			resDet.addDetalle(det);
		            }
			 } catch (SQLException ex) {
		            ex.printStackTrace();
		     }
			System.out.println("cobertura: "+tieneCovertura);
//			if(!tieneCovertura)
//				resDet.addDetalle(null);
		return resDet;
		
	}
	
	public Map<String, String> getAfiliadoCOD(String cod){
		Map<String, String> respuesta=null;
		 Connection connection = ConnectionOracleFactory2.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            String sql="SELECT UN_RUC FROM STM_ENTIDAD where "
	            		+ "CO_ENTIDAD_N='" +cod+"'";
	            ResultSet rs = stmt.executeQuery(sql);
	            if(rs.next())
	            {
	            	return respuesta;
	            }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return respuesta;
		
	}
	public Map<String, String> getAfiliacion(String dni){
		Map<String, String> respuesta=null;
		 Connection connection = ConnectionOracleFactory2.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            String sql="SELECT UN_RUC FROM STM_ENTIDAD where "
	            		+ "CO_ENTIDAD_N='" +dni+"'";
	            ResultSet rs = stmt.executeQuery(sql);
	            if(rs.next())
	            {
	            	return respuesta;
	            }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return respuesta;
		
	}
	
	public void modificarFecha(){
		 Connection connection = ConnectionOracleFactory2.getConnection();
		 try {
	            Statement stmt = connection.createStatement();
	            Long time=System.currentTimeMillis();
//	            String sql="Update STD_MONEDA set DE_MONEDA='"+time+"'";
//	            Timestamp tm= new Timestamp(time);
	            String sql="Update STD_MONEDA set DE_MONEDA='"+time+"ff'";
	            
	            stmt.executeUpdate(sql);
//	        	PreparedStatement st = connection.prepareStatement("DELETE FROM mytable WHERE columnfoo = ?");
//	        	st.setInt(1, foovalue);
//	        	int rowsDeleted = st.executeUpdate();
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	}
	public List<InConNom271Detalle> getInConNom271Detalle(String apPaterno,String apMaterno,String nombres){
		
		System.out.println("entrando a consultar getInConNom271Detalle:" 
					+" / "+apPaterno+" / "+apMaterno+" / "+nombres);
		List<InConNom271Detalle> list= new ArrayList<>();
		Connection connection = ConnectionOracleFactory2.getConnection();
		InConNom271Detalle resDet=null;
		String sql="select af.AP_PATERNO,af.NO_NOMBRES,af.CO_AFILIADO_CIP,af.AP_MATERNO,af.IsActive, "
				+ "af.COD,af.NU_TIPO_DOCUMENTO,af.NU_CONTRATO,af.PROD_CODIGO,af.CO_FILIACION,	"
				+ "af.NU_PLAN,CO_GENERO, COALESCE(to_char (af.FE_NACIMIENTO, 'yyyymmdd'),''), af.CO_ESTADO_CIVIL, af.NU_RUC, "
				+ "con.AP_CONTRATANTE, con.AM_CONTRATANTE, con.NO_CONTRATANTE, con.CO_TIPOCONTRATANTE,con.CO_TIPODOC	"
				+ "from ACRTM_AFILIADOS af "
				+ "left join STD_CONTRATANTE con on con.UN_RUC=af.NU_RUC "
				+ "where  (af.AP_PATERNO like ? or af.NU_TIPO_DOCUMENTO like ? )";
		String whe=" ";
		
		if(apMaterno.length()>0){
			whe=whe+" and af.AP_MATERNO like ? ";
		}
		if(nombres.length()>0){
			whe=whe+" and af.NO_NOMBRES like ? ";
		}
		sql=sql+whe;
		int pos=1;
		 try {
			 	PreparedStatement st = connection.prepareStatement(sql);
	        	st.setString(pos, apPaterno+"%");
	        	pos++;
	        	st.setString(pos, apPaterno+"%");
	        	pos++;
	        	if(apMaterno.length()>0){
	        		st.setString(pos, apMaterno+"%");
	        		pos++;
	    		}
	    		if(nombres.length()>0){
	    			st.setString(pos, nombres+"%");
	    			pos++;
	    		}
	            ResultSet rs = st.executeQuery();
	            while(rs.next())
	            {
	            	System.out.println(rs.getString(1));
	            	System.out.println(rs.getString(17));
	            	System.out.println(rs.getString(19));
	            	System.out.println(rs.getString(20));
	            	resDet=new InConNom271Detalle();
	        			
        			resDet.setCaPaciente("1");
        			resDet.setApPaternoPaciente(rs.getString(1));
        			resDet.setNoPaciente(rs.getString(2));
        			resDet.setCoAfPaciente(rs.getString(3));
        			
        			resDet.setApMaternoPaciente(rs.getString(4));
        			if(rs.getString(5).equals("Y"))
        				resDet.setCoEsPaciente("1"); //ESTADO
        			else
        				resDet.setCoEsPaciente("6"); //ESTADO
        			resDet.setTiDoPaciente(rs.getString(6)); //18
        			resDet.setNuDoPaciente(rs.getString(7));//19
        			resDet.setNuContratoPaciente(rs.getString(8));//20
        			resDet.setCoProducto(rs.getString(9));//21
//        			resDet.setCoProducto("2");//21
//        			resDet.setCoProducto("06");//21
        			resDet.setCoDescripcion("busqueda01");
        			resDet.setNuSCTR("001");//23
        			resDet.setCoParentesco("01");//24
        			if(rs.getString(11)==null)
        				resDet.setNuPlan("");//25
        			else
        				resDet.setNuPlan(rs.getString(11));//25
        			if(rs.getString(13).length()==0)
        				resDet.setFeNacimiento("20001010");//26
        			else
        				resDet.setFeNacimiento(rs.getString(13));//26
        			
        			resDet.setGenero(rs.getString(12));//27
        			resDet.setEsMarital(rs.getString(14));//28
        			resDet.setTiCaContratante(rs.getString(19));//29
        			resDet.setNoPaContratante(rs.getString(16));//30
        			resDet.setNoContratante(rs.getString(18));//31
        			resDet.setNoMaContratante(rs.getString(17));//32
        			resDet.setTiDoContratante(rs.getString(20));//33
        			resDet.setIdReContratante("XX5");//34
        			resDet.setCoReContratante(rs.getString(15));//35
//        			resDet.setca
        			list.add(resDet);
	        		
	            }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }

		if(list.size()==0)
			return null;
		return list;
	}
	
	public List<In278ResCGDetalle> getIn278ResCGDetalle(String apPaterno,String apMaterno,String nombres){
		
		List<In278ResCGDetalle> list= new ArrayList<>();
		Connection connection = ConnectionOracleFactory2.getConnection();
		In278ResCGDetalle resDet=null;
		String sql="select AP_PATERNO,NO_NOMBRES,CO_AFILIADO_CIP,AP_MATERNO,IsActive, "
				+ "COD,NU_TIPO_DOCUMENTO,NU_CONTRATO,PROD_CODIGO,CO_FILIACION,	"
				+ "NU_PLAN,CO_GENERO "
				+ "from ACRTM_AFILIADOS ";
//		String whe="where  AP_PATERNO = ? ";
//			}
//		}
//		
		 try {
			 	PreparedStatement st = connection.prepareStatement(sql);
//	        	st.setString		(0, apPaterno);
	            ResultSet rs = st.executeQuery();
	            while(rs.next())
	            {
	            	
	            	resDet=new In278ResCGDetalle();
	        			
        			resDet.setCaPaciente("1");
        			resDet.setApPaternoPaciente(rs.getString(1));
        			resDet.setNoPaciente(rs.getString(2));
//        			resDet.setCoAfPaciente(rs.getString(3));
        			resDet.setCoAfPaciente("326589700");
        			resDet.setApMaternoPaciente(rs.getString(4));
        			resDet.setCoTiDoPaciente("1");
        			resDet.setNuDoPaciente(rs.getString(7));//19
        			resDet.setMonPago("50");
        			resDet.setTiCaContratante("2");
        			resDet.setNoPaContratante("ICM PACHAPAQUI SAC");//30
        			resDet.setNoContratante("ICM PACHAPAQUI SAC");//31
        			resDet.setNoMaContratante("ICM PACHAPAQUI SAC");//32
        			resDet.setTiDoContratante("6");//33
        			resDet.setIdCaReContratante("XX5");
        			resDet.setNuCaReContratante("22222222");
        			resDet.setDeCarGarantia("DES");
        			resDet.setNuSoCarGarantia("123456");
        			resDet.setNuCarGarantia("1234567");
        			resDet.setVeCarGarantia("V1");
        			if(rs.getString(5).equals("Y"))
        				resDet.setEsCarGarantia("1"); //ESTADO
        			else
        				resDet.setEsCarGarantia("6"); //ESTADO
        			resDet.setCoProducto("1");//21
        			resDet.setCoProcedimiento("900152");
        			resDet.setDeProcedimiento("DES PRO");
        			resDet.setNuPlan("111");
        			resDet.setTiPlanSalud("7");
        			resDet.setCoMoneda("1");
        			resDet.setFeCarGarantia("20081021");
        			
        			list.add(resDet);
	        		
	            }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }

		if(list.size()==0)
			return null;
		return list;
	}
	public List<InConProc271Detalle> getInConProc271Detalle(String apPaterno,String apMaterno,String nombres){
		
		List<InConProc271Detalle> list= new ArrayList<>();
		Connection connection = ConnectionOracleFactory2.getConnection();
		InConProc271Detalle resDet=null;
		String sql="select AP_PATERNO,NO_NOMBRES,CO_AFILIADO_CIP,AP_MATERNO,IsActive, "
				+ "COD,NU_TIPO_DOCUMENTO,NU_CONTRATO,PROD_CODIGO,CO_FILIACION,	"
				+ "NU_PLAN,CO_GENERO "
				+ "from ACRTM_AFILIADOS ";
//		String whe="where  AP_PATERNO = ? ";
//			}
//		}
//		
		 try {
			 	PreparedStatement st = connection.prepareStatement(sql);
//	        	st.setString		(0, apPaterno);
	            ResultSet rs = st.executeQuery();
	            while(rs.next())
	            {
	            	
	            	resDet=new InConProc271Detalle();
	        			
        			resDet.setCoInProcedimiento("66");
        			resDet.setCoInRestriccion("1");
        			resDet.setCoProcedimiento("9000");
        			resDet.setImDeducible("60.5");
        			resDet.setPoCuExDecimal("0.5");
        			resDet.setNuFrecuencia("20");
        			resDet.setCoSexo("1");
        			resDet.setTiNuDias("1");
        			resDet.setTeMsgObservacion("obs");
        			
        			resDet.setIdFuentePE("PE");
        			resDet.setCoTiEspera("B30.9");
        			resDet.setDeTiEspera("de");
        			resDet.setFeFinVigencia("20171010");
        			resDet.setTeMsgTiEspera("obs");
        			resDet.setIdFuenteTE("1");
        			resDet.setCoExCarencia("");
        			resDet.setDeExCarencia("");
        			resDet.setTeMsgExCarencia("");
        			resDet.setIdFuenteEC("");
        			
        			list.add(resDet);
	        		
	            }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }

		if(list.size()==0)
			return null;
		return list;
	}
	public List<InConMed271Detalle> getInConMed271Detalle(String codCIP){
		
		List<InConMed271Detalle> list= new ArrayList<>();
		Connection connection = ConnectionOracleFactory2.getConnection();
		InConMed271Detalle resDet=null;
		System.out.println("detalle de condicion.. "+codCIP);
		String sql="select af.AP_PATERNO,af.NO_NOMBRES,af.CO_AFILIADO_CIP,af.AP_MATERNO,af.IsActive, "
				+ "af.COD,af.NU_TIPO_DOCUMENTO,af.NU_CONTRATO,af.PROD_CODIGO,af.CO_FILIACION,	"
				+ "af.NU_PLAN,CO_GENERO, COALESCE(to_char (af.FE_NACIMIENTO, 'yyyymmdd'),''), af.CO_ESTADO_CIVIL, af.NU_RUC, "
				+ "con.AP_CONTRATANTE, con.AM_CONTRATANTE, con.NO_CONTRATANTE, con.CO_TIPOCONTRATANTE,con.CO_TIPODOC,"
				+ "af.CO_AFILIADO_CIP	"
				+ "from ACRTM_AFILIADOS af "
				+ "left join STD_CONTRATANTE con on con.UN_RUC=af.NU_RUC "
				+ "where  af.CO_AFILIADO_CIP=?";
//		String whe="where  AP_PATERNO = ? ";
//			}
//		}
//		
		 try {
			 	PreparedStatement st = connection.prepareStatement(sql);
	        	st.setString		(1, codCIP);
	            ResultSet rs = st.executeQuery();
	            while(rs.next())
	            {
	            	
	            	resDet=new InConMed271Detalle();
	        			
        			resDet.setCoRestriccion("Pr");
        			resDet.setCoSeCIE10("2");
        			resDet.setDePreexistencia("detalle");
        			resDet.setEsCobertura("1");
        			resDet.setIdFuenteRE("CIE10");
        			resDet.setMoDiagnostico("50");
        			resDet.setMsgObsPr("observacion");
        			resDet.setTiEspera("20080321");
        			
        			list.add(resDet);
	        		
	            }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }

		if(list.size()==0)
			return null;
		return list;
	}
}
