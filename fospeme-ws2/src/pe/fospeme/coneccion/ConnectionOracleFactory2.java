package pe.fospeme.coneccion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.postgresql.Driver;

public class ConnectionOracleFactory2 {

	public static final String URL = "jdbc:oracle:thin:@10.64.3.25:1522/FOSPEBD";
    public static final String USER = "U_IAFAS";
    public static final String PASS = "IAFAS$2016";
    /**
     * Get a connection to database
     * @return Connection object
     */
    public static Connection getConnection()
    {
    	System.out.println("-------- Oracle JDBC Connection Testing ------");

        try {

            Class.forName("oracle.jdbc.driver.OracleDriver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();
            return null;

        }

        try {
            return DriverManager.getConnection(URL, USER, PASS);

        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return null;

        }
    	
    }
    public static void main(String[] args) {
        Connection connection = ConnectionOracleFactory1.getConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select afild_nombres from afiliado WHERE ROWNUM<=10" );
            while(rs.next())
            {
            	System.out.println(rs.getString(1));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        
    }
}
