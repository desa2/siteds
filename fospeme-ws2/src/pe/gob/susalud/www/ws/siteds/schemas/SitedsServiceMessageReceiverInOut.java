
/**
 * SitedsServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
        package pe.gob.susalud.www.ws.siteds.schemas;

        /**
        *  SitedsServiceMessageReceiverInOut message receiver
        */

        public class SitedsServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        SitedsServiceSkeleton skel = (SitedsServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("getConsultaRegAfiliados".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosResponse getConsultaRegAfiliadosResponse57 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getConsultaRegAfiliadosResponse57 =
                                                   
                                                   
                                                         skel.getConsultaRegAfiliados(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getConsultaRegAfiliadosResponse57, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getConsultaRegAfiliados"));
                                    } else 

            if("getConsultaAsegNom".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomResponse getConsultaAsegNomResponse59 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getConsultaAsegNomResponse59 =
                                                   
                                                   
                                                         skel.getConsultaAsegNom(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getConsultaAsegNomResponse59, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getConsultaAsegNom"));
                                    } else 

            if("getConsultaDatosAdi".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiResponse getConsultaDatosAdiResponse61 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getConsultaDatosAdiResponse61 =
                                                   
                                                   
                                                         skel.getConsultaDatosAdi(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getConsultaDatosAdiResponse61, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getConsultaDatosAdi"));
                                    } else 

            if("getNumAutorizacion".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionResponse getNumAutorizacionResponse63 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getNumAutorizacionResponse63 =
                                                   
                                                   
                                                         skel.getNumAutorizacion(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getNumAutorizacionResponse63, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getNumAutorizacion"));
                                    } else 

            if("getConsultaDeriva".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaResponse getConsultaDerivaResponse65 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getConsultaDerivaResponse65 =
                                                   
                                                   
                                                         skel.getConsultaDeriva(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getConsultaDerivaResponse65, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getConsultaDeriva"));
                                    } else 

            if("getConsultaSCTR".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRResponse getConsultaSCTRResponse67 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getConsultaSCTRResponse67 =
                                                   
                                                   
                                                         skel.getConsultaSCTR(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getConsultaSCTRResponse67, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getConsultaSCTR"));
                                    } else 

            if("getCondicionMedica".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaResponse getCondicionMedicaResponse69 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getCondicionMedicaResponse69 =
                                                   
                                                   
                                                         skel.getCondicionMedica(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getCondicionMedicaResponse69, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getCondicionMedica"));
                                    } else 

            if("getFoto".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetFotoResponse getFotoResponse71 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetFotoRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetFotoRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetFotoRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getFotoResponse71 =
                                                   
                                                   
                                                         skel.getFoto(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getFotoResponse71, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getFoto"));
                                    } else 

            if("getConsultaxCartaGarantia".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaResponse getConsultaxCartaGarantiaResponse73 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getConsultaxCartaGarantiaResponse73 =
                                                   
                                                   
                                                         skel.getConsultaxCartaGarantia(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getConsultaxCartaGarantiaResponse73, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getConsultaxCartaGarantia"));
                                    } else 

            if("getRegistroDecAccidente".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteResponse getRegistroDecAccidenteResponse75 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getRegistroDecAccidenteResponse75 =
                                                   
                                                   
                                                         skel.getRegistroDecAccidente(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getRegistroDecAccidenteResponse75, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getRegistroDecAccidente"));
                                    } else 

            if("getConsultaEntVinculada".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaResponse getConsultaEntVinculadaResponse77 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getConsultaEntVinculadaResponse77 =
                                                   
                                                   
                                                         skel.getConsultaEntVinculada(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getConsultaEntVinculadaResponse77, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getConsultaEntVinculada"));
                                    } else 

            if("getConsultaAsegCod".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodResponse getConsultaAsegCodResponse79 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getConsultaAsegCodResponse79 =
                                                   
                                                   
                                                         skel.getConsultaAsegCod(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getConsultaAsegCodResponse79, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getConsultaAsegCod"));
                                    } else 

            if("getConsultaObservacion".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionResponse getConsultaObservacionResponse81 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getConsultaObservacionResponse81 =
                                                   
                                                   
                                                         skel.getConsultaObservacion(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getConsultaObservacionResponse81, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getConsultaObservacion"));
                                    } else 

            if("getConsultaProc".equals(methodName)){
                
                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcResponse getConsultaProcResponse83 = null;
	                        pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcRequest wrappedParam =
                                                             (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcRequest)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcRequest.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getConsultaProcResponse83 =
                                                   
                                                   
                                                         skel.getConsultaProc(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getConsultaProcResponse83, false, new javax.xml.namespace.QName("http://www.susalud.gob.pe/ws/siteds/schemas",
                                                    "getConsultaProc"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetFotoRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetFotoRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetFotoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetFotoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcRequest param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcRequest.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosResponse wrapgetConsultaRegAfiliados(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomResponse wrapgetConsultaAsegNom(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiResponse wrapgetConsultaDatosAdi(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionResponse wrapgetNumAutorizacion(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaResponse wrapgetConsultaDeriva(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRResponse wrapgetConsultaSCTR(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaResponse wrapgetCondicionMedica(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetFotoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetFotoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetFotoResponse wrapgetFoto(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetFotoResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetFotoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaResponse wrapgetConsultaxCartaGarantia(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteResponse wrapgetRegistroDecAccidente(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaResponse wrapgetConsultaEntVinculada(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodResponse wrapgetConsultaAsegCod(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionResponse wrapgetConsultaObservacion(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcResponse wrapgetConsultaProc(){
                                pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcResponse wrappedElement = new pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetFotoRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetFotoRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetFotoResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetFotoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteRequest.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteRequest.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteResponse.class.equals(type)){
                
                        return pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    