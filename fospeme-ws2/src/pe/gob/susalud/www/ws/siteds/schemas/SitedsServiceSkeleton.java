
/**
 * SitedsServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
package pe.gob.susalud.www.ws.siteds.schemas;

import java.util.List;
import java.util.Map;

import com.sun.jmx.snmp.Timestamp;

import pe.fospeme.util.ServiceWS;
import pe.gob.susalud.jr.transaccion.susalud.bean.In271ConDtad;
import pe.gob.susalud.jr.transaccion.susalud.bean.In271ConObs;
import pe.gob.susalud.jr.transaccion.susalud.bean.In278ResCG;
import pe.gob.susalud.jr.transaccion.susalud.bean.In278ResCGDetalle;
import pe.gob.susalud.jr.transaccion.susalud.bean.In278SolCG;
import pe.gob.susalud.jr.transaccion.susalud.bean.In997ResAut;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConAse270;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConCod271;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConCod271Detalle;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConEntVinc278;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConMed271;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConMed271Detalle;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConNom271;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConNom271Detalle;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConProc271;
import pe.gob.susalud.jr.transaccion.susalud.bean.InConProc271Detalle;
import pe.gob.susalud.jr.transaccion.susalud.bean.InResEntVinc278;
import pe.gob.susalud.jr.transaccion.susalud.bean.InSolAut271;
import pe.gob.susalud.jr.transaccion.susalud.util.ConAse270_ToBean;
import pe.gob.susalud.jr.transaccion.susalud.util.ConCod271_ToX12N;
import pe.gob.susalud.jr.transaccion.susalud.util.ConEntVinc278_ToBean;
import pe.gob.susalud.jr.transaccion.susalud.util.ConMed271_ToX12N;
import pe.gob.susalud.jr.transaccion.susalud.util.ConNom271_ToX12N;
import pe.gob.susalud.jr.transaccion.susalud.util.In271ConDtad_ToXs12N;
import pe.gob.susalud.jr.transaccion.susalud.util.In271ConObs_ToBean;
import pe.gob.susalud.jr.transaccion.susalud.util.In271ConObs_ToX12N;
import pe.gob.susalud.jr.transaccion.susalud.util.In271ConProc_ToX12N;
import pe.gob.susalud.jr.transaccion.susalud.util.In278ResCG_ToBean;
import pe.gob.susalud.jr.transaccion.susalud.util.In278ResCG_ToX12N;
import pe.gob.susalud.jr.transaccion.susalud.util.In278SolCG_ToBean;
import pe.gob.susalud.jr.transaccion.susalud.util.In997ResAut_ToBean;
import pe.gob.susalud.jr.transaccion.susalud.util.In997ResAut_ToXs12N;
import pe.gob.susalud.jr.transaccion.susalud.util.ResEntVinc278_ToX12N;
import pe.gob.susalud.jr.transaccion.susalud.util.SolAut271_ToBean;

/**
 * SitedsServiceSkeleton java skeleton for the axisService
 */
public class SitedsServiceSkeleton {

	/**
	 * Auto generated method signature
	 * 
	 * @param getConsultaRegAfiliadosRequest
	 * @return getConsultaRegAfiliadosResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosResponse getConsultaRegAfiliados(
			pe.gob.susalud.www.ws.siteds.schemas.GetConsultaRegAfiliadosRequest getConsultaRegAfiliadosRequest) {
		// TODO : fill this with the necessary business logic
		System.out.println("entrando a getConsultaRegAfiliados");
		throw new java.lang.UnsupportedOperationException(
				"Please implement " + this.getClass().getName() + "#getConsultaRegAfiliados");
	}

	/**
	 * Auto generated method signature
	 * IMPLEMENTADO
	 * @param getConsultaAsegNomRequest
	 * @return getConsultaAsegNomResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomResponse getConsultaAsegNom(
			pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegNomRequest getConsultaAsegNomRequest) {
		// TODO : fill this with the necessary business logic
		System.out.println("entrando a getConsultaAsegNom");
		
		
//		transformando TramaConsulta a objeto
		InConAse270 req=null;
		try {
			req=ConAse270_ToBean.traducirEstructura270(getConsultaAsegNomRequest.getTxPeticion());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error al traducir");
		}
		
		String exceptionValor="0000";
//		contruyendo objeto de salida
		InConNom271 res= new InConNom271();
		res.setNoTransaccion("271_CON_NOM");
		System.out.println(req.getIdReceptor());
		System.out.println(req.getIdRemitente());
		System.out.println("----------");
		//de tabla CO_ENTIDAD_N
		ServiceWS sw= new ServiceWS();
		boolean existe=sw.getIafas(req.getIdRemitente());
		System.out.println(existe);
		String tramaRespuesta="";
		if(existe){
			res.setIdRemitente(req.getIdReceptor());
			res.setIdReceptor(req.getIdRemitente());
			res.setFeTransaccion(req.getFeTransaccion());
			res.setHoTransaccion(req.getHoTransaccion());
			res.setIdCorrelativo(req.getIdCorrelativo());
			res.setIdTransaccion("271");
			res.setTiFinalidad("11");
			res.setCaRemitente("2");
			res.setCaReceptor("2");
			String busquedaAPDNI=req.getApPaternoPaciente();
			
			if(req.getApPaternoPaciente().length()==0){
				busquedaAPDNI="%"+req.getNuDocumento();
			}
			
			
			String respuesta=sw.getRUCRemitente(req.getIdRemitente());
			res.setNuRucReceptor(respuesta);
			List<InConNom271Detalle> list=sw.getInConNom271Detalle(
					busquedaAPDNI, req.getApMaternoPaciente(), req.getNoPaciente());
			
			if(list!=null){
				for (int i = 0; i < list.size(); i++) {
					res.addDetalle(list.get(i));
				}
			}else{
				exceptionValor="1300";
			}
			tramaRespuesta=ConNom271_ToX12N.traducirEstructura271(res);
		}else{
			exceptionValor="0440";
			tramaRespuesta="";
		}

		
		System.out.println("---");
		System.out.println(exceptionValor);
		
//		transformado Objeto a trama de salida
		
//		Generando Respuesta
		GetConsultaAsegNomResponse response=new GetConsultaAsegNomResponse();
		response.setCoError(exceptionValor);
		response.setCoIafa(req.getIdReceptor());
		response.setTxNombre("271_CON_NOM");
		response.setTxRespuesta(tramaRespuesta);
		
		return response;
	}

	/**
	 * Auto generated method signature
	 * IMPLEMENTADO
	 * @param getConsultaDatosAdiRequest
	 * @return getConsultaDatosAdiResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiResponse getConsultaDatosAdi(
			pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDatosAdiRequest getConsultaDatosAdiRequest) {
		// TODO : fill this with the necessary business logic
		System.out.println("entrando a getConsultaDatosAdi");
		InConAse270 req=null;
		try {
			req=ConAse270_ToBean.traducirEstructura270(getConsultaDatosAdiRequest.getTxPeticion());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error al traducir");
		}
		
		ServiceWS sw= new ServiceWS();
//		contruyendo objeto de salida
		
		In271ConDtad res= new In271ConDtad();
		
		res.setNoTransaccion("271_CON_DTAD");
		res.setIdRemitente(req.getIdReceptor());
		res.setIdReceptor(req.getIdRemitente());
		res.setFeTransaccion(req.getFeTransaccion());
		res.setHoTransaccion(req.getHoTransaccion());
		res.setIdCorrelativo(req.getIdCorrelativo());
		res.setIdTransaccion("271");
		res.setTiFinalidad("11");
		res.setCaRemitente("2");
		res.setCaReceptor("2");
		res.setNuRucReceptor("20100054184");
		
		res.setCaPaciente("1");
		res.setApPaternoPaciente("AP PAT");
		res.setNoPaciente("NOMBRES");
		res.setCoAfPaciente("2225");
		res.setApMaternoPaciente("AP MAT");
		
		res.setDeDirPaciente1("dir1");
		res.setDeDirPaciente2("dir2");
		res.setCoUbigeoPaciente("0011");
		res.setNoContacto("contacto");
		res.setEmContacto("email");
		
		String tramaRespuesta=In271ConDtad_ToXs12N.traducirEstructura278ConDtad(res);
		
//		Generando Respuesta
		GetConsultaDatosAdiResponse response=new GetConsultaDatosAdiResponse();
		response.setCoError("0000");//ok
		response.setCoIafa("10006");
		response.setTxNombre("271_CON_DTAD");
		response.setTxRespuesta(tramaRespuesta);
		return response;
	}

	/**
	 * Auto generated method signature
	 * IMPLEMENTADO
	 * @param getNumAutorizacionRequest
	 * @return getNumAutorizacionResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionResponse getNumAutorizacion(
			pe.gob.susalud.www.ws.siteds.schemas.GetNumAutorizacionRequest getNumAutorizacionRequest) {
		// TODO : fill this with the necessary business logic
		System.out.println("entrando a getNumAutorizacion");
		
		InSolAut271 req=null;
		
		try {
			req=SolAut271_ToBean.traducirEstructura271(getNumAutorizacionRequest.getTxPeticion());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error al traducir");
		}
		
		ServiceWS sw= new ServiceWS();
//		contruyendo objeto de salida
		
		In997ResAut res= new In997ResAut();
		
		res.setNoTransaccion("997_RES_AUT");
		System.out.println("**************");
		System.out.println(req.getFeTransaccion());
		System.out.println(req.getHoTransaccion());
		res.setIdRemitente(req.getIdReceptor());
		res.setIdReceptor(req.getIdRemitente());
		res.setFeTransaccion(req.getFeTransaccion());
		res.setHoTransaccion(req.getHoTransaccion());
		
//		res.setFeTransaccion("12/12/2017");
		res.setHoTransaccion("15:30");
		
//		res.setIdCorrelativo(req.getIdCorrelativo());
		res.setIdCorrelativo("0004");
		
		res.setIdTransaccion("997");
//		Timestamp hoy= new Timestamp(System.currentTimeMillis());
		Long cor=System.currentTimeMillis();
		String dd=""+cor;
		dd=dd.substring(5, 13);
		res.setNuAutorizacion(dd);
		res.setCoSeguridad("09ab");
		res.setCoError("A");
		res.setInCoErroEncontrado("001");
		
		
		String tramaRespuesta=In997ResAut_ToXs12N.traducirEstructura997ResAut(res);
		
//		Generando Respuesta
		GetNumAutorizacionResponse response=new GetNumAutorizacionResponse();
		response.setCoError("0000");//ok
		response.setCoIafa("10006");
		response.setTxNombre("997_RES_AUT");
		response.setTxRespuesta(tramaRespuesta);
		return response;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getConsultaDerivaRequest
	 * @return getConsultaDerivaResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaResponse getConsultaDeriva(
			pe.gob.susalud.www.ws.siteds.schemas.GetConsultaDerivaRequest getConsultaDerivaRequest) {
		// TODO : fill this with the necessary business logic
		System.out.println("entrando a getConsultaDeriva");
		throw new java.lang.UnsupportedOperationException(
				"Please implement " + this.getClass().getName() + "#getConsultaDeriva");
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getConsultaSCTRRequest
	 * @return getConsultaSCTRResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRResponse getConsultaSCTR(
			pe.gob.susalud.www.ws.siteds.schemas.GetConsultaSCTRRequest getConsultaSCTRRequest) {
		// TODO : fill this with the necessary business logic
		System.out.println("entrando a getConsultaSCTR");
		throw new java.lang.UnsupportedOperationException(
				"Please implement " + this.getClass().getName() + "#getConsultaSCTR");
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getCondicionMedicaRequest
	 * @return getCondicionMedicaResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaResponse getCondicionMedica(
			pe.gob.susalud.www.ws.siteds.schemas.GetCondicionMedicaRequest getCondicionMedicaRequest) {
		// TODO : fill this with the necessary business logic
		System.out.println("entrando a getCondicionMedica");
		
		InConAse270 req=null;
		
		try {
			req=ConAse270_ToBean.traducirEstructura270(getCondicionMedicaRequest.getTxPeticion());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error al traducir");
		}
		
		ServiceWS sw= new ServiceWS();
//		contruyendo objeto de salida
		
		InConMed271 res= new InConMed271();
		
		res.setNoTransaccion("271_CON_MED");
		res.setIdRemitente(req.getIdReceptor());
		res.setIdReceptor(req.getIdRemitente());
		res.setFeTransaccion(req.getFeTransaccion());
		res.setHoTransaccion(req.getHoTransaccion());
		res.setIdCorrelativo(req.getIdCorrelativo());
		res.setIdTransaccion("271");
		res.setTiFinalidad("11");
		res.setCaRemitente("2");
		res.setCaReceptor("2");
		res.setNuRucReceptor("20508650451");
		
//		res=ws.getDetalleIn278SolCGD(res,req);
		List<InConMed271Detalle> list=sw.getInConMed271Detalle(
				req.getCoAfPaciente());
		
		if(list!=null){
			for (int i = 0; i < list.size(); i++) {
				res.addDetalle(list.get(i));
			}
		}
		
		String tramaRespuesta=ConMed271_ToX12N.traducirEstructura271(res);
		
//		Generando Respuesta
		GetCondicionMedicaResponse response=new GetCondicionMedicaResponse();
		response.setCoError("0000");//ok
		response.setCoIafa("10006");
		response.setTxNombre("278_RES_CG");
		response.setTxRespuesta(tramaRespuesta);
		return response;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getFotoRequest
	 * @return getFotoResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetFotoResponse getFoto(
			pe.gob.susalud.www.ws.siteds.schemas.GetFotoRequest getFotoRequest) {
		// TODO : fill this with the necessary business logic
		System.out.println("entrando a getFoto");
		throw new java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getFoto");
	}

	/**
	 * Auto generated method signature
	 * IMPLEMENTADO
	 * @param getConsultaxCartaGarantiaRequest
	 * @return getConsultaxCartaGarantiaResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaResponse getConsultaxCartaGarantia(
			pe.gob.susalud.www.ws.siteds.schemas.GetConsultaxCartaGarantiaRequest getConsultaxCartaGarantiaRequest) {
		// TODO : fill this with the necessary business logic
		System.out.println("entrando a getConsultaxCartaGarantia");
		
		
		In278SolCG req=null;
		
		try {
			req=In278SolCG_ToBean.traducirEstructura278Sol(getConsultaxCartaGarantiaRequest.getTxPeticion());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error al traducir");
		}
		
		ServiceWS sw= new ServiceWS();
//		contruyendo objeto de salida
		
		In278ResCG res= new In278ResCG();
		
		res.setNoTransaccion("278_RES_CG");
		res.setIdRemitente(req.getIdReceptor());
		res.setIdReceptor(req.getIdRemitente());
		res.setFeTransaccion(req.getFeTransaccion());
		res.setHoTransaccion(req.getHoTransaccion());
		res.setIdCorrelativo(req.getIdCorrelativo());
		res.setIdTransaccion("278");
		res.setTiFinalidad("11");
		res.setCaRemitente("2");
		res.setCaReceptor("2");
		res.setNuRucReceptor("20100054184");
		
//		res=ws.getDetalleIn278SolCGD(res,req);
		List<In278ResCGDetalle> list=sw.getIn278ResCGDetalle(
				req.getApPaternoPaciente(), req.getApMaternoPaciente(), req.getNoPaciente());
		
		if(list!=null){
			for (int i = 0; i < list.size(); i++) {
				res.addDetalle(list.get(i));
			}
		}
		
		String tramaRespuesta=In278ResCG_ToX12N.traducirEstructura278Res(res);
		
//		Generando Respuesta
		GetConsultaxCartaGarantiaResponse response=new GetConsultaxCartaGarantiaResponse();
		response.setCoError("0000");//ok
		response.setCoIafa("10006");
		response.setTxNombre("278_RES_CG");
		response.setTxRespuesta(tramaRespuesta);
		return response;
		
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getRegistroDecAccidenteRequest
	 * @return getRegistroDecAccidenteResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteResponse getRegistroDecAccidente(
			pe.gob.susalud.www.ws.siteds.schemas.GetRegistroDecAccidenteRequest getRegistroDecAccidenteRequest) {
		// TODO : fill this with the necessary business logic
		System.out.println("entrando a getRegistroDecAccidente");
		throw new java.lang.UnsupportedOperationException(
				"Please implement " + this.getClass().getName() + "#getRegistroDecAccidente");
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getConsultaEntVinculadaRequest
	 * @return getConsultaEntVinculadaResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaResponse getConsultaEntVinculada(
			pe.gob.susalud.www.ws.siteds.schemas.GetConsultaEntVinculadaRequest getConsultaEntVinculadaRequest) {
		
//		abre conexion
		ServiceWS sw= new ServiceWS();
		InConEntVinc278 req=null;
		
//		transformando TramaConsulta a objeto
		try {
			req=ConEntVinc278_ToBean.traducirEstructura271(getConsultaEntVinculadaRequest.getTxPeticion());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error al traducir");
		}
		String exceptionValor="0000";
//		contruyendo objeto de salida
		InResEntVinc278 res= new InResEntVinc278();
		res.setNoTransaccion("278_RES_ENT_VINC");
		
		String respuesta=sw.getIdRemitente(req.getIdRemitente());
		res.setIdRemitente(req.getIdReceptor());
		res.setIdReceptor(req.getIdRemitente());
		res.setFeTransaccion(req.getFeTransaccion());
		res.setHoTransaccion(req.getHoTransaccion());
		res.setIdCorrelativo(req.getIdCorrelativo());
		res.setIdTransaccion("278");
		res.setTiFinalidad("11");
		if(respuesta!=null){
			if(respuesta.equals("2"))
				res.setRespuesta("Y");//si es ipres si
			else
				res.setRespuesta("N");//si es ipres si
		}else{
			res.setRespuesta("Y");//si es ipres si
		}
		
		res.setMsgRespuesta("ok");
			
//		transformado Objeto a trama de salida
		String tramaRespuesta=ResEntVinc278_ToX12N.traducirEstructura271(res);
		
//		Generando Respuesta
		GetConsultaEntVinculadaResponse response=new GetConsultaEntVinculadaResponse();
		response.setCoError(exceptionValor);//ok
		response.setCoIafa("10006");
		response.setTxNombre("278_RES_ENT_VINC");
		response.setTxRespuesta(tramaRespuesta);
		return response;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getConsultaAsegCodRequest
	 * @return getConsultaAsegCodResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodResponse getConsultaAsegCod(
			pe.gob.susalud.www.ws.siteds.schemas.GetConsultaAsegCodRequest getConsultaAsegCodRequest) {
		// TODO : fill this with the necessary business logic
		System.out.println("entrando a getConsultaAsegCod");
//		transformando TramaConsulta a objeto
		System.out.println(getConsultaAsegCodRequest.getTxPeticion());
		InConAse270 req=null;
		try {
			req=ConAse270_ToBean.traducirEstructura270(getConsultaAsegCodRequest.getTxPeticion());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error al traducir");
		}
		
		ServiceWS sw= new ServiceWS();
//		contruyendo objeto de salida
		
		InConCod271 res= new InConCod271();
		
		res.setNoTransaccion("271_CON_COD");
		res.setIdRemitente(req.getIdReceptor());
		res.setIdReceptor(req.getIdRemitente());
		res.setFeTransaccion(req.getFeTransaccion());
		res.setHoTransaccion(req.getHoTransaccion());
		res.setIdCorrelativo(req.getIdCorrelativo());
		res.setIdTransaccion("271");
		res.setTiFinalidad("11");
		res.setCaRemitente("2");
		
//		res.setUserRemitente(userRemitente);
//		res.setPassRemitente(passRemitente);
//		res.setFeUpFoto(feUpFoto);
		res.setCaReceptor("2");
		
		String respuesta=sw.getRUCRemitente(req.getIdRemitente());
		
		res.setNuRucReceptor(respuesta);
		res.setCaPaciente("1");
		res=sw.getAfiliadoCOD(res,req.getCoAfPaciente());
		
		
//		transformado Objeto a trama de salida
		String tramaRespuesta=ConCod271_ToX12N.traducirEstructura271(res);
		
//		Generando Respuesta
		GetConsultaAsegCodResponse response=new GetConsultaAsegCodResponse();
		response.setCoError("0000");
		response.setCoIafa("10006");
		response.setTxNombre("271_CON_COD");
		response.setTxRespuesta(tramaRespuesta);

		
		return response;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getConsultaObservacionRequest
	 * @return getConsultaObservacionResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionResponse getConsultaObservacion(
			pe.gob.susalud.www.ws.siteds.schemas.GetConsultaObservacionRequest getConsultaObservacionRequest) {
		// TODO : fill this with the necessary business logic
		System.out.println("entro a getConsultaObservacion222");
		InConAse270 req=null;
		
		try {
			req=ConAse270_ToBean.traducirEstructura270(getConsultaObservacionRequest.getTxPeticion());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error al traducir");
		}
		
		ServiceWS sw= new ServiceWS();
//		contruyendo objeto de salida
		
		In271ConObs res= new In271ConObs();
		
		res.setNoTransaccion("271_CON_OBS");
		res.setIdRemitente(req.getIdReceptor());
		res.setIdReceptor(req.getIdRemitente());
		res.setFeTransaccion(req.getFeTransaccion());
		res.setHoTransaccion(req.getHoTransaccion());
		res.setIdCorrelativo(req.getIdCorrelativo());
		res.setIdTransaccion("271");
		res.setTiFinalidad("11");
		res.setCaRemitente("2");
		res.setCaReceptor("2");
		res.setNuRucReceptor("20100054184");
		res.setCaPaciente("1");
		res.setApPaternoPaciente("aaaap");
		res.setNoPaciente("na5aao");
		res.setCoAfPaciente("22555");
		res.setApMaternoPaciente("maaaat");
		res.setCoSubCobertura("4");
		res.setTeMsgLibre1("1111111");
		res.setTeMsgLibre2("2222222222222");
//		res=ws.getDetalleIn278SolCGD(res,req);
		String tramaRespuesta="";
		try{
			tramaRespuesta=In271ConObs_ToX12N.traducirEstructura278ConObs(res);
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("error al traducir respuest");
		}
		
		
//		Generando Respuesta
		GetConsultaObservacionResponse response=new GetConsultaObservacionResponse();
		response.setCoError("0000");//ok
		response.setCoIafa("10006");
		response.setTxNombre("271_CON_OBS");
		response.setTxRespuesta(tramaRespuesta);
		response.setRptObs("555555");
		return response;
	}

	/**
	 * Auto generated method signature
	 * IMPLEMENTADO
	 * @param getConsultaProcRequest
	 * @return getConsultaProcResponse
	 */

	public pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcResponse getConsultaProc(
			pe.gob.susalud.www.ws.siteds.schemas.GetConsultaProcRequest getConsultaProcRequest) {
		// TODO : fill this with the necessary business logic
		System.out.println("entro a getConsultaProc");

		InConAse270 req=null;
		
		try {
			req=ConAse270_ToBean.traducirEstructura270(getConsultaProcRequest.getTxPeticion());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error al traducir");
		}
		
		ServiceWS sw= new ServiceWS();
//		contruyendo objeto de salida
		
		InConProc271 res= new InConProc271();
		
		res.setNoTransaccion("271_CON_PROC");
		res.setIdRemitente(req.getIdReceptor());
		res.setIdReceptor(req.getIdRemitente());
		res.setFeTransaccion(req.getFeTransaccion());
		res.setHoTransaccion(req.getHoTransaccion());
		res.setIdCorrelativo(req.getIdCorrelativo());
		res.setIdTransaccion("278");
		res.setTiFinalidad("11");
		res.setCaRemitente("2");
		res.setCaReceptor("2");
		res.setNuRucReceptor("20100054184");
		res.setCaPaciente("1");
		res.setApPaternoPaciente("ap");
		res.setNoPaciente("no");
		res.setCoAfPaciente("22555");
		res.setApMaternoPaciente("mat");
//		res.setpro
//		res=ws.getDetalleIn278SolCGD(res,req);
		List<InConProc271Detalle> list=sw.getInConProc271Detalle(
				req.getApPaternoPaciente(), req.getApMaternoPaciente(), req.getNoPaciente());
		
		if(list!=null){
			for (int i = 0; i < list.size(); i++) {
				res.addDetalle(list.get(i));
			}
		}
		
		String tramaRespuesta=In271ConProc_ToX12N.traducirEstructura278ConProc(res);
		
//		Generando Respuesta
		GetConsultaProcResponse response=new GetConsultaProcResponse();
		response.setCoError("0000");//ok
		response.setCoIafa("10006");
		response.setTxNombre("271_CON_PROC");
		response.setTxRespuesta(tramaRespuesta);
		return response;
		
	}

}
